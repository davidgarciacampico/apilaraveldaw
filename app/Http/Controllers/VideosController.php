<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Videos;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\Comentarios;

class VideosController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        try {
            $videos=Videos::all();
            return response()->json(array('msg' => $videos, 'status' => 'success'),200);
        } catch (\Exception $e) {
            return response()->json(array('msg' => 'No hay registros disponibles', 'status' => 'false'),400);
        }
    }


    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request)
    {
        try {
            $videos = new Videos;
      
            $image = $request->file('image');
            $path = $image->store('public');
            
            $videos->user_id = $request->input('user_id');
            $videos->title = $request->input('title');
            $videos->description = $request->input('description');
            $videos->status = $request->input('status');
            $videos->image = $image;
            $videos->video_path = Storage::url($path);          
            $videos->save();
            
            return response()->json(array('msg' => 'Insertado correctamente', 'status' => 'success'),200);
        } catch (\Exception $e) {
            return response()->json(array('msg' => 'No se ha podido insertar el registro', 'status' => 'false'),400);
        }
        
        
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        try {
            return $videos = Videos::findOrFail($id);
        }catch (\Exception $e) {
            return response()->json(array('msg' => 'No se ha encontrado el registro', 'status' => 'false'),400);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        try {
            $videos =Videos::findOrFail($id);

            $image = $request->file('image');
            $path = $image->store('public');
            
            $videos->user_id = $request->input('user_id');
            $videos->title = $request->input('title');
            $videos->description = $request->input('description');
            $videos->status = $request->input('status');
            $videos->image = $image;
            $videos->video_path = Storage::url($path);   
           
            $videos->save();
            
            return response()->json(array('msg' => 'Actualizado correctamente', 'status' => 'success'),200);
        } catch (\Exception $e) {
            return response()->json(array('msg' => 'No se ha podido actualizar el registro', 'status' => 'false'),400);
        }
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        try {
            $videos=Videos::findOrFail($id);
            $videos->delete();
            return response()->json(array('msg' => 'Eliminado correctamente', 'status' => 'success'),200);
        } catch (\Exception $e) {
            return response()->json(array('msg' => 'No se ha podido eliminar el registro', 'status' => 'false'),400);
        }
    }
}
