<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Comentarios;
use App\Models\Videos;


use Illuminate\Support\Facades\Hash;



class UsersController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        try {
            $users=User::all();
            return response()->json(array('msg' => $users, 'status' => 'success'),200);
        } catch (\Exception $e) {
            return response()->json(array('msg' => 'No hay registros disponibles', 'status' => 'false'),400);
        }
    }
    

    public function showUserVideo($id){
        $users = User::with('videos')->find($id);
        return response()->json(array('msg' => $users, 'status' => 'success'),200);
    }

    public function showUserComments($id){
        $users = User::with('comentarios')->find($id);
        return response()->json(array('msg' => $users, 'status' => 'success'),200);
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request)
    {
        try {
            $users = new User;
            
            $users->role = $request->input('role');
            $users->name = $request->input('name');
            $users->surname = $request->input('surname');
            $users->email = $request->input('email');
            $users->password = Hash::make(($request->input('password')));
            $users->image = $request->file('image');   
       
            $users->save();
            
            return response()->json(array('msg' => 'Insertado correctamente', 'status' => 'success'),200);
        } catch (\Exception $e) {
            return response()->json(array('msg' => 'No se ha podido insertar el registro', 'status' => 'false'),400);
        }
        
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        try {
            return $users = User::findOrFail($id);
        }catch (\Exception $e) {
            return response()->json(array('msg' => 'No se ha encontrado el registro', 'status' => 'false'),400);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        try {
            $users =User::findOrFail($id);

            $users->role = $request->input('role');
            $users->name = $request->input('name');
            $users->surname = $request->input('surname');
            $users->email = $request->input('email');
            $users->password = Hash::make(($request->input('password')));
            $users->image = $request->file('image');   

            $users->save();
            
            return response()->json(array('msg' => 'Actualizado correctamente', 'status' => 'success'),200);
        } catch (\Exception $e) {
           
            return response()->json(array('msg' => 'No se ha podido actualizar el registro', 'status' => 'false'),400);
        }
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        try {
            $users=User::findOrFail($id);
            $users->delete();
            return response()->json(array('msg' => 'Eliminado correctamente', 'status' => 'success'),200);
        } catch (\Exception $e) {
            return response()->json(array('msg' => 'No se ha podido eliminar el registro', 'status' => 'false'),400);
        }
    }
}
