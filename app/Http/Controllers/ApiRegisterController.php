<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class ApiRegisterController extends Controller
{
    
    public function login(Request $request){
       $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
       ]);

       if(!auth()->attempt($loginData)){
            return response()->json(['msg' => 'Error al iniciar sesion', 'status' => 400]);
       }
       $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response(['user' => auth()->user(), 'access_token' => $accessToken, 'status' => 200]);
    }


    public function register(Request $request){
     
         try {

            $validateData = $request->validate([
                'role' => 'required|max:120',
                'name' => 'required|max:70',
                'surname' => 'required|max:70',
                'email' => 'email|required',
                'password' => 'required|confirmed',
                'image' => 'required|max:255'

            ]);
            $validateData['password'] = Hash::make($request->password);

            $user = User::create($validateData);
            $accessToken = $user->createToken('authToken')->accessToken;

            return response(['user' => $user, 'access_token' => $accessToken, 'status' => 200]);
         } catch (\Exception $e) {
             return response(['msg' => 'No es posible realizar el registro', 'status' => 400]);

        }
    }

    public function logout(Request $request)
    {
        
        $request->user()->token()->revoke();

        return response()->json(['msg' => 'Desconectado correctamente', 'status' => 200]);
    }

    public function user(Request $request)
    {
        return response()->json([$request->user(), 'status' => 200]);
    }
}
