<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Auth\Middleware\Role as Middleware;

use Illuminate\Support\Facades\Auth;


class Api
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, String $role)
    {   
        $user = Auth::user();
        if($user->role == $role){
            return $next($request);
        }
        
        return redirect('/home');
    }
    
}
