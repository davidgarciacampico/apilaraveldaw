<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Models\Videos;
use App\Http\Models\User;

class Comentarios extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'video_id', 'body', 
    ];

    public function user(){
        return $this->belongsTo('\App\Models\User');
    }

    public function videos(){
        return $this->belongsTo('\App\Models\videos');
    }
    
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
