<?php

namespace App\Models;


use Illuminate\Contacts\Auth\MustverifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'role', 'name', 'surname', 'email', 'password','image',
    ];


    public function videos(){
        return $this->hasMany('\App\Models\videos');
    }

    public function comentarios(){
        return $this->hasMany('\App\Models\Comentarios');
    }

    protected $hidden = [
        'created_at', 'updated_at', 'remember_token'
    ];
}
