<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Models\Comentarios;
use App\Http\Models\User;


class Videos extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'title', 'description','status', 'image', 'video_path' 
    ];

    public function user(){
        return $this->hasMany('\App\Models\User');
    }

    public function comentarios(){
        return $this->belongsTo('\App\Models\Comentarios');
    }

    protected $hidden = [
        'created_at', 'updated_at'
    ];

}
