<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix'=>'users', 'middleware' => 'auth:api' ],function(){
    Route::get('read', "UsersController@index");
    Route::get('videos/{id}', 'UsersController@showUserVideo');
    Route::get('comments/{id}', 'UsersController@showUserComments');
    Route::delete('delete/{id}', "UsersController@destroy")->middleware('api:admin');
    Route::post('update/{id}','UsersController@update')->middleware('api:admin');
    Route::post('store', 'UsersController@store');
    Route::get('show/{id}','UsersController@show');
    Route::post('create','UsersController@create')->middleware('api:admin');
});

Route::group(['prefix'=>'comments',  'middleware' => 'auth:api' ],function(){
    Route::get('read', "ComentariosController@index");
    Route::delete('delete/{id}', "ComentariosController@destroy")->middleware('api:admin');
    Route::post('update/{id}','ComentariosController@update')->middleware('api:admin');
    Route::post('store', 'ComentariosController@store');
    Route::get('show/{id}','ComentariosController@show');
    Route::post('create','ComentariosController@create')->middleware('api:admin');
});
Route::group(['prefix'=>'videos', 'middleware' => 'auth:api'],function(){
    Route::get('read', "VideosController@index");
    Route::delete('delete/{id}', "VideosController@destroy")->middleware('api:admin');
    Route::post('update/{id}','VideosController@update')->middleware('api:admin');
    Route::post('store', 'VideosController@store');
    Route::get('show/{id}','VideosController@show');
    Route::post('create','VideosController@create')->middleware('api:admin');
});



Route::post('auth/register', "ApiRegisterController@register");
Route::post('auth/login', "ApiRegisterController@login");
Route::middleware(['auth:api'])->get('auth/logout', "ApiRegisterController@logout");
Route::middleware(['auth:api'])->get('auth/user', "ApiRegisterController@user");

