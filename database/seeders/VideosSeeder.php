<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Str;
class VideosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('videos')->insert([
            [
                'user_id' => '1',
                'title' => Str::random(10),
                'description' => Str::random(10),
                'status' => Str::random(10),
                'image' => Str::random(10),
                'video_path' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'user_id' => '2',
                'title' => Str::random(10),
                'description' => Str::random(10),
                'status' => Str::random(10),
                'image' => Str::random(10),
                'video_path' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'user_id' => '3',
                'title' => Str::random(10),
                'description' => Str::random(10),
                'status' => Str::random(10),
                'image' => Str::random(10),
                'video_path' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'user_id' => '4',
                'title' => Str::random(10),
                'description' => Str::random(10),
                'status' => Str::random(10),
                'image' => Str::random(10),
                'video_path' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'user_id' => '5',
                'title' => Str::random(10),
                'description' => Str::random(10),
                'status' => Str::random(10),
                'image' => Str::random(10),
                'video_path' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'user_id' => '6',
                'title' => Str::random(10),
                'description' => Str::random(10),
                'status' => Str::random(10),
                'image' => Str::random(10),
                'video_path' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'user_id' => '7',
                'title' => Str::random(10),
                'description' => Str::random(10),
                'status' => Str::random(10),
                'image' => Str::random(10),
                'video_path' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],

           
            ]);
    }
}
