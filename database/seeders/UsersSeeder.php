<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'role' => 'user',
                'name' => Str::random(10),
                'surname' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('1234'),
                'image' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'role' => 'user',
                'name' => Str::random(10),
                'surname' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('1234'),
                'image' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'role' => 'user',
                'name' => Str::random(10),
                'surname' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('1234'),
                'image' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'role' => 'user',
                'name' => Str::random(10),
                'surname' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('1234'),
                'image' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'role' => 'user',
                'name' => Str::random(10),
                'surname' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('1234'),
                'image' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'role' => 'user',
                'name' => Str::random(10),
                'surname' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('1234'),
                'image' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'role' => 'user',
                'name' => Str::random(10),
                'surname' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('1234'),
                'image' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'role' => 'user',
                'name' => Str::random(10),
                'surname' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('1234'),
                'image' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'role' => 'user',
                'name' => Str::random(10),
                'surname' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('1234'),
                'image' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'role' => 'user',
                'name' => Str::random(10),
                'surname' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'password' => Hash::make('1234'),
                'image' => Str::random(10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            ]);
    }
}
